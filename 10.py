'''10. Напишите функцию tuple_sort(), которая сортирует кортеж, состоящий из целых чисел по возрастанию и возвращает его.'''

def tuple_sort():
    return tuple(sorted(list(some_tuple)))

some_tuple = (1, 21, 321, 0, -4)
print(tuple_sort())
