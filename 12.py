'''12. Написать функцию date, принимающую 3 аргумента — день, месяц и год. Вернуть True, если такая дата есть в нашем календаре, и False иначе.
'''

import datetime
def date(day, month, year):
    try:
        datetime.date(year, month, day)
        return True
    except:
        return False

print(date(29,2,2022))