'''2. Декоратор считающий время выполнения функции.'''

import time

def timer(fun):
    def wrapped():
        start_time = time.perf_counter_ns()
        result = fun()
        print('Время выполнения функции:', time.perf_counter_ns() - start_time, 'нс')
        return result
    return wrapped

@timer
def foo():
    return '...Результат функции...'

print(foo())
