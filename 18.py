class Plant:
    title = 'plant in the forest'

    def __init__(self, kind, color):
        self.kind = kind
        self.color = color

    def __str__(self):
        return f'Plant kind: {self.kind}, color: {self.color}'  

    def change_color(self, new_color):
        self.color = new_color
        print(f'New color: {new_color}')   

a = Plant('bracken', 'red')
b = Plant('moss', 'blue')
print(a == b)
print(a.color)
a.title = 'PLANT'
print(a.change_color('yellow'))
print(a)

class Cactus(Plant):
    def __init__(self, kind='cactus', color="green"):
        self.kind = kind
        self.color = color

class Birch(Plant):
    def change_color(self):
        super().change_color('red')
        print(f'New color: {self.color}')

cactus = Cactus()
birch = Birch('birch', 'white')

print(type(cactus))
print(isinstance(cactus, Plant))
birch.change_color()
print(birch)