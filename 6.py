'''6. Написать функцию, которая сливает два словаря в один.'''

def merge_dictionaries(dictionary_one, dictionary_two):
    return dictionary_one | dictionary_two

dictionary_one = {1:'a', 2:'b', 3:'c'}
dictionary_two = {3:'3', 4:'4', 5:'5', 6:6}

print(merge_dictionaries(dictionary_one, dictionary_two))