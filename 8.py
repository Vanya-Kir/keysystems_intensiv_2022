'''8. Напишите функцию reverse_my_list(lst), которая принимает список и меняет местами его первый и последний элемент.'''

def reverse_my_list(lst):
    if (len(lst) > 0):
        lst[0], lst[-1] = lst[-1], lst[0]
    return lst

print(reverse_my_list([0, 1, 2, 3]))