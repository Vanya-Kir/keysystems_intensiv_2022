'''4. Посчитайте, сколько раз символ встречается в строке.'''

symbol = 'a'
text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam faucibus diam nec posuere viverra. In sed enim blandit, volutpat mauris et, mollis libero. Aliquam ac lorem at nulla vehicula lacinia vitae sit amet lectus. Etiam iaculis, mi ut fermentum finibus, ex nisi eleifend lacus, vitae lacinia diam justo in sem.'

print(f'Символ "{symbol}" встречается в строке \n{text} \n{text.count(symbol)} раз.')