'''16. Напишите функцию sum_range(a, z), которая перемножает все целые числа от значения a до величины z включительно'''

def sum_range(a, z):
    result = 1
    for i in range(a, z + 1):
        result = result * i 
    return result

print(sum_range(2, 5))